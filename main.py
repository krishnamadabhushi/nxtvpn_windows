import os
import sys

sys.path.insert(0, os.path.join(os.path.split(os.path.abspath(__file__))[0], "libs"))

from kivy.factory import Factory
from kivy.config import Config

Config.set("graphics", "width", "1200")
Config.set("graphics", "height", "700")
Config.set(
    "kivy",
    "window_icon",
    os.path.join(os.path.split(os.path.abspath(__file__))[0], "data", "logo.png"),
)

from kivy.app import App
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.core.window import Window

from kivymd.theming import ThemeManager


class NXTVPN(App):
    theme_cls = ThemeManager()
    theme_cls.primary_palette = "Green"
    theme_cls.primary_hue = "700"
    theme_cls.accent_palette = "Indigo"
    icon = "data/logo.png"
    title = "NXTVPN"

    def build(self):
        self.load_all_kv_files(os.path.join(self.directory, "libs", "uix", "kv"))
        Window.bind(on_resize=self.window_resize)
        self.window_left = Window.left
        return Factory.StartScreen()

    def window_resize(self, instance_window, width_window, height_window):
        if width_window < 1200:
            Window.size = (1200, 700)
            Window.left = self.window_left

    def on_start(self):
        def _set_elements_width(interval):
            self.root.ids.scr_mngr.get_screen("login").set_elements_width()

        Clock.schedule_once(_set_elements_width, 0)

    def load_all_kv_files(self, directory_kv_files):
        for kv_file in os.listdir(directory_kv_files):
            kv_file = os.path.join(directory_kv_files, kv_file)
            if os.path.isfile(kv_file) and kv_file.endswith("kv"):
                with open(kv_file, encoding="utf-8") as kv:
                    Builder.load_string(kv.read())


NXTVPN().run()
