import socket


def get_connect_string(host="8.8.8.8", port=53, timeout=3):
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return '[color=#72d41bff]• online[/color]'
    except Exception:
        return '[color=#f5313dff]• offline[/color]'
