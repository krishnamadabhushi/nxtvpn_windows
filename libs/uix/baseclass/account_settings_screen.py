from kivy.uix.screenmanager import Screen

from libs.kivymd.theming import ThemableBehavior


class AccountSettingsScreen(ThemableBehavior, Screen):
    def open_list_user_emails(self, *args):
        pass

    def event_on_text(self, instance_field, instance_textinput, value):
        if value[-1:] == "\t":
            instance_textinput.text = instance_textinput.text[:-1]
            instance_textinput.focus = False
            if instance_field is self.ids.label_first_name:
                self.ids.label_last_name.ids.field.focus = True
            elif instance_field is self.ids.label_last_name:
                self.ids.label_mail.ids.field.focus = True
            elif instance_field is self.ids.label_mail:
                self.ids.label_number.ids.field.focus = True
            elif instance_field is self.ids.label_number:
                self.ids.label_adress.ids.field.focus = True
            elif instance_field is self.ids.label_adress:
                self.ids.label_new_password.ids.field.focus = True
            elif instance_field is self.ids.label_new_password:
                self.ids.label_retype_password.ids.field.focus = True
            elif instance_field is self.ids.label_retype_password:
                self.ids.label_list_mail.ids.field.focus = True
