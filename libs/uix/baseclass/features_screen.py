from kivy.uix.screenmanager import Screen
from libs.uix.baseclass.components import FeaturesItem


class FeaturesScreen(Screen):
    def on_enter(self):
        for item in self.get_features_list():
            self.ids.box.add_widget(
                FeaturesItem(icon=item[0], text=item[1], title=item[2])
            )

    def on_leave(self):
        self.ids.box.clear_widgets()

    def get_features_list(self):
        features_list = (
            [
                "/Users/macbookair/Projects/NXTVPN/data/dedicated-vpn-server.png",
                "Connect to a dedicated VPN server which is exclusive to you. Avoid noisy neighbor that slow up the server. A dedicated server ensures you do not have performance or bandwidth issue and you can always connect confidently without worrying about server performance.",
                "Dedicated VPN Server",
            ],
            [
                "data/dedicated-ip-server.png",
                "Get dedicated IPv4 address dedicated to you for your VPN session. The dedicated IPv4 Address is exclussive to your VPN session and it is not shared by any other user. This way you can circumvent geo restrictions for your streaming content and also avoid website restrictions.",
                "Dedicated IPv4/v6",
            ],
            [
                "data/multi-hop-vpn.png",
                "Add extra layer of security and anonimity with multiple VPNs. In this arrangement your data will be passed via multiple VPN hops and tunnels. This will ensure that  if any person or organization is tracking you, they will have to trace back via multiple tunnels and multiple vpn nodes, each having own security hardening. We provide upto 5 hops via multiple VPN nodes, making internet presence highly secure.",
                "Multi-Hop VPN",
            ],
        )
        return features_list
