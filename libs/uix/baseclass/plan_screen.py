from kivy.uix.screenmanager import Screen

from libs.kivymd.theming import ThemableBehavior


COLOR_GREY = [0.27058823529411763, 0.30196078431372547, 0.396078431372549, 1]
COLOR_LILAC = [0.0705882352941176, 0.0705882352941176, 0.1490196078431372, 1]


class PlanScreen(ThemableBehavior, Screen):
    def press_on_plan(self, instance_plan):
        for widget in self.ids.box_plan.children:
            if widget.border == "data/border-green.png":
                widget.border = "data/border.png"
                widget.color_select = COLOR_GREY
                break

        instance_plan.border = "data/border-green.png"
        instance_plan.color_select = self.theme_cls.primary_color

    def press_on_enabled_item(self, enabled_item):
        for widget in self.ids.enabled_box.children:
            if widget.color_select == self.theme_cls.primary_color:
                widget.color_select = COLOR_GREY
                widget.image_check = "data/check-mark-white.png"
                break

        enabled_item.image_check = "data/check-mark-green.png"
        enabled_item.color_select = self.theme_cls.primary_color
