from kivy.uix.screenmanager import Screen


class PasswordSuccessScreen(Screen):
    def set_elements_width(self, app, interval=0):
        width = app.root.ids.scr_mngr.get_screen("login").ids.button_login.width
        self.ids.button.width = width
