from kivy.properties import ListProperty, ObjectProperty, StringProperty
from kivy.uix.modalview import ModalView

from libs.kivymd.theming import ThemableBehavior


class BaseMacOSDialog(ThemableBehavior, ModalView):
    canvas_color = ListProperty()
    callback = ObjectProperty(lambda x: None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not self.canvas_color:
            self.canvas_color = self.theme_cls.primary_color


class CheckingForUpdateDialog(BaseMacOSDialog):
    pass


class ReportDialog(BaseMacOSDialog):
    def event_on_text(self, instance_field, instance_textinput, value):
        if value[-1:] == "\t":
            instance_textinput.text = instance_textinput.text[:-1]
            instance_textinput.focus = False
            if instance_field is self.ids.user_name_report:
                self.ids.email_report.ids.field.focus = True
            elif instance_field is self.ids.email_report:
                self.ids.text_report.ids.field.focus = True
            elif instance_field is self.ids.text_report:
                self.ids.attach_report.ids.field.focus = True


class UpdateDialog(BaseMacOSDialog):
    new_version_program = StringProperty("3.7.5")
