from kivy.metrics import dp
from kivy.uix.screenmanager import Screen

from libs.uix.baseclass.float_error import FloatError


class LoginScreen(Screen, FloatError):
    current_focus_field = None

    def set_elements_width(self):
        width = self.ids.label_login.width + dp(100)
        self.ids.button_login.width = width
        self.ids.email_field.width = width
        self.ids.email_password.width = width

    def event_on_focus(self, instance_field, instance_textinput, focus_value):
        self.current_focus_field = instance_field

    def event_on_text(self, instance_field, instance_textinput, value):
        if value[-1:] == "\t":
            instance_textinput.text = instance_textinput.text[:-1]
            if instance_field is self.ids.email_field:
                instance_textinput.focus = False
                self.ids.email_password.ids.field.focus = True
            elif instance_field is self.ids.email_password:
                instance_textinput.focus = False
                self.ids.email_field.ids.field.focus = True
