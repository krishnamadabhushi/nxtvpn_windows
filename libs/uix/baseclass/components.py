from kivy.properties import StringProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout


class BoxTab(BoxLayout):
    name_item = StringProperty()
    color = ListProperty()
    color_text = ListProperty()


class PlanItem(BoxLayout):
    text_item = StringProperty()
    color_select = ListProperty()
    border = StringProperty()


class CorrectlyEnabledItem(BoxLayout):
    text_item = StringProperty()
    image_check = StringProperty()
    color_select = ListProperty()


class FeaturesItem(BoxLayout):
    text = StringProperty()
    title = StringProperty()
    icon = StringProperty()
