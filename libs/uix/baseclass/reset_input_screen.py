from kivy.uix.screenmanager import Screen
from kivy.utils import get_hex_from_color

from libs.uix.baseclass.float_error import FloatError


class ResetInputScreen(Screen, FloatError):
    def set_elements_width(self, app, interval=0):
        width = app.root.ids.scr_mngr.get_screen("login").ids.button_login.width
        self.ids.button_reset.width = width
        self.ids.email_field.width = width

    def set_user_email(self, app):
        self.ids.label_info.text = (
            "Please input a code sent to [color={}]{}[/color] below\nor "
            "follow the instruction link to reset your password".format(
                get_hex_from_color(app.theme_cls.primary_color),
                self.get_user_email(app),
            )
        )

    def get_user_email(self, app):
        user_email = app.root.ids.scr_mngr.get_screen("reset").ids.email_field.text
        return user_email
