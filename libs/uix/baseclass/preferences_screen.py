from kivy.uix.screenmanager import Screen

from libs.kivymd.theming import ThemableBehavior
from uix.baseclass.components import BoxTab


class PreferencesScreen(ThemableBehavior, Screen):
    def press_on_item_tab(self, *args):
        color_grey = [0.5411764705882353, 0.5647058823529412, 0.6352941176470588, 1]
        color_white = [1, 1, 1, 1]
        instance_item = args[0][0]

        for widget in self.ids.box.children:
            if issubclass(widget.__class__, BoxTab):
                if widget.ids.name_item.color == color_white:
                    widget.ids.name_item.color = color_grey
                    widget.ids.separator.custom_color = [0, 0, 0, 0]
                    break

        instance_item.parent.ids.name_item.color = color_white
        instance_item.parent.ids.separator.custom_color = self.theme_cls.primary_color
        if self.ids.preferences_manager.has_screen(args[0][1]):
            self.ids.preferences_manager.current = args[0][1]
