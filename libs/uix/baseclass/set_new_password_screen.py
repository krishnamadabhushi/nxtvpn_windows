from kivy.uix.screenmanager import Screen

from libs.uix.baseclass.float_error import FloatError


class SetNewPasswordScreen(Screen, FloatError):
    def set_elements_width(self, app, interval=0):
        width = app.root.ids.scr_mngr.get_screen("login").ids.button_login.width
        self.ids.button_password.width = width
        self.ids.email_password.width = width
