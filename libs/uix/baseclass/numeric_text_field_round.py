from kivy.clock import Clock
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import (
    NumericProperty,
    ListProperty,
    ObjectProperty,
    StringProperty,
)
from kivy.uix.boxlayout import BoxLayout

from libs.kivymd.theming import ThemableBehavior

Builder.load_string(
    """
#:import MDLabel libs.kivymd.label.MDLabel


<Field@TextInput>
    background_active: f'{images_path}transparent.png'
    background_normal: f'{images_path}transparent.png'
    background_disabled_normal: f'{images_path}transparent.png'
    disabled_foreground_color: 1, 1, 1, 1
    multiline: False
    padding_y: dp(7)
    disabled: True
    input_filter: root.parent.parent.input_filter
    cursor_color: root.parent.parent.cursor_color
    foreground_color: root.parent.parent.foreground_color
    selection_color: root.parent.parent.selection_color
    do_backspace: root.parent.parent.do_backspace
    font_size: '15px'
    font_name: 'data/fonts/Lato/Lato-Regular.ttf'
    on_focus:
        root.parent.parent._current_color = root.parent.parent.active_color \
        if self.focus else root.parent.parent.normal_color
        root.parent.parent.get_color_line(self, self.text, self.focus)
        if root.parent.parent.event_focus: root.parent.parent.event_focus(root, self, self.focus)
    on_text: root.parent.parent.check_input(root, root.text)


<BlankPoint@Label>
    text: '-'
    font_size: '28sp'
    size_hint_x: None
    width: self.texture_size[0]
    color: color_grey


<NumericTextFieldRound>
    orientation: 'vertical'
    size_hint: None, None
    height: dp(48)
    padding: 10, 0, 10, 0
        
    canvas:
        Color:
            rgba: root._current_color
        RoundedRectangle:
            size: self.size
            pos: self.pos
            radius: [25,]
    canvas.after:
        Color:
            rgba: root._outline_color
        Line:
            width: 1.1
            rounded_rectangle:
                (self.x, self.y, self.width, self.height,\
                25, 25, 25, 25,\
                self.height)

    BoxLayout:
        id: box
        size_hint_x: None
        width: self.minimum_width
        pos_hint: {'center_x': .5}

        Field:
            id: field_1
            size_hint_x: None
            width: '28'
            font_size: '28sp'
        
        BlankPoint:

        Field:
            id: field_2
            size_hint_x: None
            width: '28'
            font_size: '28sp'
        
        BlankPoint:

        Field:
            id: field_3
            size_hint_x: None
            width: '28'
            font_size: '28sp'
        
        BlankPoint:

        Field:
            id: field_4
            size_hint_x: None
            width: '28'
            font_size: '28sp'
            
"""
)


class NumericTextFieldRound(ThemableBehavior, BoxLayout):
    width = NumericProperty(Window.width - dp(100))
    """Text field width."""

    text = StringProperty()
    """Text of field."""

    selection_color = ListProperty()
    """Text selection color."""

    active_color = ListProperty([1, 1, 1, 0.2])
    """The color of the text field when it is in focus."""

    normal_color = ListProperty([1, 1, 1, 0.5])
    """The color of the text field when it not in focus."""

    foreground_color = ListProperty([1, 1, 1, 1])
    """Text color."""

    cursor_color = ListProperty()
    """Color of cursor"""

    event_focus = ObjectProperty()
    """The function is called at the moment of focus/unfocus of the text field.
    """

    _current_color = ListProperty()

    _outline_color = ListProperty([0, 0, 0, 0])

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.focus = False
        if not (len(self.cursor_color)):
            self.cursor_color = self.theme_cls.primary_color
        if not (len(self.selection_color)):
            self.selection_color = self.theme_cls.primary_color
            self.selection_color[3] = 0.75
        self._current_color = self.normal_color

    def on_normal_color(self, instance, value):
        self._current_color = value

    def get_color_line(self, field_inastance, field_text, field_focus):
        if not field_focus:
            self._outline_color = [0, 0, 0, 0]
        else:
            self._outline_color = self.theme_cls.primary_color

    def isnumeric(self, value):
        try:
            int(value)
            return True
        except ValueError:
            return False

    def input_filter(self, value, boolean):
        if self.isnumeric(value):
            return value
        return ""

    def check_input(self, instance_field, text):
        if text != "":
            instance_field.text = text[-1]
            instance_field.disabled = True
        field, children = self.check_fields()
        if field:
            Clock.schedule_once(lambda x: self.set_focus(field), 0.1)
        else:
            self.set_focus(children[-1])

    def get_fields(self):
        children = self.ids.box.children
        _children = children.copy()
        _children.reverse()
        _children = [
            widget for widget in _children if widget.__class__ is Factory.Field
        ]
        return _children

    def check_fields(self):
        _field = None
        _children = self.get_fields()
        for field in _children:
            if field.text == "":
                _field = field
                break
        return _field, _children

    def set_focus(self, field, interval=0):
        field.disabled = False
        field.focus = True
        self.focus = True

    def on_touch_down(self, touch):
        if self.focus:
            self.focus = False
            return
        if self.collide_point(touch.x, touch.y):
            field, children = self.check_fields()
            if field:
                Clock.schedule_once(lambda x: self.set_focus(field), 0.1)
            return super().on_touch_down(touch)

    def do_backspace(self, *args):
        _field = None
        _children = self.get_fields()
        for field in _children:
            if field.focus:
                _field = field
                index = _children.index(field) - 1
                _children[index].text = ""
                break
