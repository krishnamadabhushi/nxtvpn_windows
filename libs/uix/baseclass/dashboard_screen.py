from kivy.animation import Animation
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.metrics import dp
from kivy.properties import StringProperty, ListProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.screenmanager import Screen

from libs.hover_behavior import HoverBehavior
from libs.kivymd.expansionpanel import MDExpansionPanel
from libs.kivymd.list import ILeftBody, OneLineAvatarListItem
from libs.kivymd.theming import ThemableBehavior
from libs.uix.baseclass.dialogs import CheckingForUpdateDialog, ReportDialog


COLOR_LILAC_LIGHT = [0, 0, 0, .2980392156862745]
COLOR_LILAC = [0.09019607843137255, 0.06666666666666667, 0.19607843137254902, 1]
COLOR_GREY = '#454d65ff'


class AvatarSampleWidget(ILeftBody, Image):
    pass


class BoxAccordionContentItem(BoxLayout):
    pass


class CustomNavigationDrawerIconButton(OneLineAvatarListItem, HoverBehavior):
    source = StringProperty()
    divider = None
    _background_color = ListProperty([0, 0, 0, 0])

    def _set_active(self, active, nav_drawer):
        pass

    def on_enter(self, *args):
        self._background_color = COLOR_LILAC_LIGHT

    def on_leave(self, *args):
        self._background_color = COLOR_LILAC


class AccordionContentItem(BoxLayout):
    name_server = StringProperty()
    name_city = StringProperty()
    background_color = ListProperty([0, 0, 0, 0])
    selected = False


class DashboardScreen(ThemableBehavior, Screen):
    app = ObjectProperty()
    _x = None
    _y = None
    _profile_sets = False
    _cache = []

    def on_enter(self):
        self.ids.info_box.ids.label_provider.text = self.get_provider_name()
        self.ids.info_box.ids.label_actual_ip.text = \
            f'[size=12][color={COLOR_GREY}]Actual IP:' \
            f'[/color] [b]{self.get_actual_ip()}[/b][/size]'

    def get_provider_name(self):
        return 'Docomono Provider'

    def get_actual_ip(self):
        return '192.186.0.1'

    def zoom_set(self, value):
        return
        map = self.ids.map
        if not self._x and not self._y:
            self._x, self._y = map.center_x, map.center_y
        map.width += value
        map.height += value
        map.center_x = self._x
        map.center_y = self._y
        # print(map.center_x, map.center_y)

    def get_server_available(self):
        return "7000"

    def quick_connect(self, method="connect"):
        # Addind connecting button
        if method == "connect":
            self.ids.box_button_quick_button.clear_widgets()
            connecting_button = Factory.DashboardConnectingButton()
            self.ids.box_button_quick_button.add_widget(connecting_button)
            connecting_button.width = dp(150)

            # Adding label "FINDING THE BEST SERVER"
            self.ids.info_box.ids.label_not_protected.text = "FINDING THE BEST SERVER"
            self.ids.info_box.ids.label_not_protected.color = self.theme_cls.primary_color
            self.ids.info_box.ids.icon.opacity = 0
        elif method == "disconnect":
            self.ids.box_button_quick_button.clear_widgets()
            connecting_button = Factory.DashboardQuickConnectButton()
            self.ids.box_button_quick_button.add_widget(connecting_button)

    def hide_profile_items(self):
        Animation(x=Window.width, d=0.2).start(self.ids.profile_items)

    def hide_user_profile(self):
        Animation(x=Window.width, d=0.2).start(self.ids.user_profile)

    def show_profile_items1(self, *args):
        print(333)

    def show_user_profile(self):
        Animation(x=Window.width - self.ids.user_profile.width, d=0.2).start(
            self.ids.user_profile
        )

    def show_profile_items(self, *args):
        Animation(x=Window.width - self.ids.profile_items.width, d=0.2).start(
            self.ids.profile_items
        )
        self.set_profile_items()

    def set_profile_items(self):
        def callback(text):
            from libs.kivymd.toast import toast

            toast(f"{text} to {content.name_item}")

        if not self._profile_sets:
            test_data = {
                "Iceland": [["Ind Server 1", "Madan"], ["Ind Server 2", "Depau"]],
                "India": [["Ind Server 1", "Shiila"], ["Ind Server 2", "Beshil"]],
                "China": [["Ind Server 1", "Pekin"], ["Ind Server 2", "Lacusta"]],
                "Russia": [["Ind Server 1", "Moscow"], ["Ind Server 2", "Jakutia"]],
                "Germany": [["Ind Server 1", "Berlin"], ["Ind Server 2", "Rammon"]],
                "Finland": [["Ind Server 1", "Beringer"], ["Ind Server 2", "Ostob"]],
            }

            for land in test_data.keys():
                box_content = BoxAccordionContentItem()
                for data in test_data[land]:
                    content = AccordionContentItem(
                        name_server=data[0], name_city=data[1]
                    )
                    box_content.add_widget(content)
                self.ids.profile_items.ids.accordion.add_widget(
                    MDExpansionPanel(
                        content=box_content,
                        icon=f"data/flags/{land.lower()}.png",
                        title=land,
                    )
                )
        self._profile_sets = True

    def callback_items_menu_profile(self, instance, name_item):
        if name_item == "Check for Update":
            CheckingForUpdateDialog().open()
        elif name_item == "Send Report":
            ReportDialog().open()
        elif name_item == "Preferences":
            self.hide_user_profile()
            self.app.root.ids.scr_mngr.current = "preferences"
        elif name_item == "Sign Out":
            self.app.root.ids.scr_mngr.current = "login"

    def set_user_profile_icons(self):
        for items in {
            "home": "Home",
            "about": "About",
            "update": "Check for Update",
            "preferences": "Preferences",
            "promo": "Promo",
            "report": "Send Report",
            "out": "Sign Out",
            "quit": "Quit",
        }.items():
            self.ids.user_profile.ids.box_item.add_widget(
                CustomNavigationDrawerIconButton(
                    text=items[1],
                    source=f"data/profile/{items[0]}.png",
                    on_press=lambda x, y=items[1]: self.callback_items_menu_profile(
                        x, y
                    ),
                )
            )

    def connect(self):
        from libs.kivymd.toast import toast

        toast("Connect...")

    def select_item_profile(self, instance_profile):
        def hide_button_connect(accordion_item):
            Animation(opacity=0, d=0.001).start(accordion_item.ids.button_connect)
            Animation(size=(0, 0), d=0.001).start(accordion_item.ids.button_connect)

        if instance_profile.selected:
            self.connect()
            return

        select_color = [
            0.12156862745098039,
            0.09019607843137255,
            0.25098039215686274,
            1,
        ]
        color_grey = [0.27058823529411763, 0.30196078431372547, 0.396078431372549, 1]

        self._cache.append(instance_profile.ids.label_name_city.text)
        instance_profile.ids.label_name_server.color = self.theme_cls.primary_color
        instance_profile.ids.label_name_city.text = ""

        for accordion_item in instance_profile.parent.children:
            if accordion_item.background_color == select_color:
                accordion_item.background_color = [
                    0.08627450980392157,
                    0.0784313725490196,
                    0.18823529411764706,
                    1,
                ]

                accordion_item.ids.button_connect.text = ""
                Clock.schedule_once(lambda x: hide_button_connect(accordion_item), 0)
                accordion_item.ids.label_name_server.color = color_grey
                accordion_item.ids.label_name_city.text = self._cache[0]
                accordion_item.selected = False
                self._cache = self._cache[1:]
                break

        instance_profile.background_color = select_color
        instance_profile.ids.button_connect.text = "Connect"
        instance_profile.ids.button_connect.size = (dp(100), dp(36))
        instance_profile.ids.button_connect.opacity = 1
        instance_profile.selected = True

    def set_locations_user(self):
        Animation(opacity=1, d=0.2).start(self.ids.im_1)
        Animation(opacity=1, d=0.2).start(self.ids.im_2)
        Animation(opacity=1, d=0.2).start(self.ids.im_3)
        Animation(opacity=1, d=0.2).start(self.ids.bubble)
