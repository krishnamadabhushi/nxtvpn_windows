from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen


class ConfirmationScreen(Screen):
    _email = StringProperty()

    def set_elements_width(self, app, interval=0):
        width = app.root.ids.scr_mngr.get_screen("login").ids.button_login.width
        self.ids.button_login_here.width = width
        self._email = self.get_email(app)

    def get_email(self, app):
        return app.root.ids.scr_mngr.get_screen("login").ids.email_field.text
