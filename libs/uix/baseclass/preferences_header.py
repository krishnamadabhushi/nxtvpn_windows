import time
import random

from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout

from libs.kivymd.toast import toast

mbs = list(range(100, 400))


class PreferencesHeader(BoxLayout):
    def get_provider_name(self):
        return 'Docomono Provider'

    def disconnect(self):
        toast("Disconnect")

    def labels_update(self):
        def label_update(interval):
            self.ids.label_time.text = "Time : [b]{}[/b]  |  ".format(
                time.strftime("%H:%M:%S", time.localtime())
            )

        def mbs_update(interval):
            self.ids.label_mbs.text = f"{random.choice(mbs)}mb/s"

        Clock.schedule_interval(label_update, 1)
        Clock.schedule_interval(mbs_update, 3)
