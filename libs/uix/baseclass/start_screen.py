from kivymd.textfields import MDTextFieldRound


class MyMDTextFieldRound(MDTextFieldRound):
    def show_password(self, field, button):
        """
        Called when you press the right button in the password field.

        instance_field: kivy.uix.textinput.TextInput object;
        instance_button: kivymd.button.MDIconButton object;

        """

        # Show or hide text of password, set focus field
        # and set icon of right button.
        field.password = not field.password
        field.focus = True
        button.icon = "eye" if button.icon == "eye-off" else "eye-off"
