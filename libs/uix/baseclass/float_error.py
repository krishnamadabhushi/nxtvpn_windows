from kivy.animation import Animation
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout


class FloatError(FloatLayout):
    _interval = 0

    def show_error_not_found_email_password(self, app, text_error):
        def wait_interval(interval):
            self._interval += interval
            if self._interval > 3:
                Clock.unschedule(wait_interval)
                self.hide_error_not_found_email_password(instance_label)
                self._interval = 0

        def start_animation(interval):
            Animation(y=Window.height - instance_label.height, d=0.2).start(
                instance_label
            )
            Clock.schedule_interval(wait_interval, 0.5)

        instance_label = self.ids.label_error_email_password
        instance_label.text = text_error
        Clock.schedule_once(start_animation, 0.5)

    def hide_error_not_found_email_password(self, instance_label):
        Animation(y=Window.height, d=0.2).start(instance_label)
