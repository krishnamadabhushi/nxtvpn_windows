from kivy.uix.screenmanager import Screen

from libs.uix.baseclass.float_error import FloatError


class ResetScreen(Screen, FloatError):
    def set_elements_width(self, app, interval=0):
        width = app.root.ids.scr_mngr.get_screen("login").ids.button_login.width
        self.ids.button_reset.width = width
        self.ids.email_field.width = width

    def reset(self, app):
        if not self.check_valid_login(self.ids.email_field.text):
            self.show_error_not_found_email_password(
                app, "Oops!! no account associated with that email"
            )
            self.ids.email_field.ids.field.focus = True
            self.ids.email_field._outline_color = self.ids.email_field.error_color
            self.ids.email_field._instance_icon_left.text_color = (
                self.ids.email_field.error_color
            )
        else:
            app.root.ids.scr_mngr.current = "reset input code"

    def check_valid_login(self, user_login):
        return True
